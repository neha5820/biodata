package com.infyniteloop.tech.biodata.service;

import com.infyniteloop.tech.biodata.model.PersonalInformation;
import com.infyniteloop.tech.biodata.repository.PersonalInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PersonalInfoServiceImpl implements PersonalInfoService{

    @Autowired
    PersonalInfoRepository repo;


    public Optional<PersonalInformation> getPersonalInfoById(Long id)
    {
        return repo.findById(id);
    }

    public List<PersonalInformation> getPersonalInfoAll()
    {
        return repo.findAll();
    }





}
