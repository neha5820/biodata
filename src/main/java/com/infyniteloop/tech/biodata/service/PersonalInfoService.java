package com.infyniteloop.tech.biodata.service;

import com.infyniteloop.tech.biodata.model.PersonalInformation;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;



public interface PersonalInfoService {

    public Optional<PersonalInformation> getPersonalInfoById(Long id);

    public List<PersonalInformation> getPersonalInfoAll();


}
