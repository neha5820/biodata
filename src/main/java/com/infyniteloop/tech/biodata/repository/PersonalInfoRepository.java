package com.infyniteloop.tech.biodata.repository;

import com.infyniteloop.tech.biodata.model.PersonalInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonalInfoRepository extends JpaRepository<PersonalInformation,Long> {
}