package com.infyniteloop.tech.biodata.controller;


import com.infyniteloop.tech.biodata.model.PersonalInformation;
import com.infyniteloop.tech.biodata.service.PersonalInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/biodata")
public class PersonalInfoController {

    @Autowired
    PersonalInfoService service;

    @RequestMapping(value = "/pi/{id}", method = RequestMethod.GET)
    public ResponseEntity<PersonalInformation> getPersonalInfoById(@PathVariable("id") Long id, HttpServletRequest request)
    {

        Optional<PersonalInformation> records = service.getPersonalInfoById(id);

        return new ResponseEntity<>(records.get(), HttpStatus.OK);

    }

    @RequestMapping(value = "/pi/getall", method = RequestMethod.GET)
    public ResponseEntity<PersonalInformation> getPersonalInfoAll( HttpServletRequest request)
    {

        List<PersonalInformation> records = service.getPersonalInfoAll();

        return new ResponseEntity(records, HttpStatus.OK);

    }
}
