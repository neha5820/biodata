package com.infyniteloop.tech.biodata.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name = "PERSONAL_INFO")
public class PersonalInformation {

    String first_name;
    String last_name;

    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    long id;

    public PersonalInformation() {

    }

    public PersonalInformation(long id, String first_name, String last_name) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


}
